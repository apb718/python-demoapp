Flask==3.0.0
py-cpuinfo==9.0.0
psutil==5.9.6
gunicorn==20.1.0
black==20.8b1
flake8==3.9.0
Jinja2==3.1.2
pytest==7.4.3
